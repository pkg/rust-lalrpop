Source: rust-lalrpop
Section: rust
Priority: optional
Build-Depends: debhelper-compat (= 13),
 dh-sequence-cargo,
 cargo:native,
 rustc:native (>= 1.70),
 libstd-rust-dev,
 librust-ascii-canvas-3-dev,
 librust-bit-set-0.8-dev,
 librust-ena-0.14-dev,
 librust-itertools-0+use-std-dev (>= 0.10.5-~~),
 librust-lalrpop-util-0.20+lexer-dev,
 librust-lalrpop-util-0.20+unicode-dev,
 librust-lalrpop-util-0.20-dev,
 librust-petgraph-0.6-dev,
 librust-pico-args-0.5-dev,
 librust-regex-1+std-dev (>= 1.3-~~),
 librust-regex-1+unicode-dev (>= 1.3-~~),
 librust-regex-syntax-0.8+unicode-dev,
 librust-regex-syntax-0.8-dev,
 librust-string-cache-0.8-dev,
 librust-term-0.7-dev,
 librust-tiny-keccak-2+default-dev (>= 2.0.2-~~),
 librust-tiny-keccak-2+sha3-dev (>= 2.0.2-~~),
 librust-unicode-xid-0.2-dev,
 librust-walkdir-2+default-dev (>= 2.4.0-~~)
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Daniel Kahn Gillmor <dkg@fifthhorseman.net>
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/lalrpop]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/lalrpop
Homepage: https://github.com/lalrpop/lalrpop
X-Cargo-Crate: lalrpop
Rules-Requires-Root: no

Package: librust-lalrpop-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-ascii-canvas-3-dev,
 librust-bit-set-0.8-dev,
 librust-ena-0.14-dev,
 librust-itertools-0+use-std-dev (>= 0.10.5-~~),
 librust-lalrpop-util-0.20+lexer-dev,
 librust-lalrpop-util-0.20+unicode-dev,
 librust-lalrpop-util-0.20-dev,
 librust-petgraph-0.6-dev,
 librust-pico-args-0.5-dev,
 librust-regex-1+std-dev (>= 1.3-~~),
 librust-regex-1+unicode-dev (>= 1.3-~~),
 librust-regex-syntax-0.8+unicode-dev,
 librust-regex-syntax-0.8-dev,
 librust-string-cache-0.8-dev,
 librust-term-0.7-dev,
 librust-tiny-keccak-2+default-dev (>= 2.0.2-~~),
 librust-tiny-keccak-2+sha3-dev (>= 2.0.2-~~),
 librust-unicode-xid-0.2-dev,
 librust-walkdir-2+default-dev (>= 2.4.0-~~)
Provides:
 librust-lalrpop+default-dev (= ${binary:Version}),
 librust-lalrpop+lexer-dev (= ${binary:Version}),
 librust-lalrpop+pico-args-dev (= ${binary:Version}),
 librust-lalrpop+unicode-dev (= ${binary:Version}),
 librust-lalrpop-0-dev (= ${binary:Version}),
 librust-lalrpop-0+default-dev (= ${binary:Version}),
 librust-lalrpop-0+lexer-dev (= ${binary:Version}),
 librust-lalrpop-0+pico-args-dev (= ${binary:Version}),
 librust-lalrpop-0+unicode-dev (= ${binary:Version}),
 librust-lalrpop-0.20-dev (= ${binary:Version}),
 librust-lalrpop-0.20+default-dev (= ${binary:Version}),
 librust-lalrpop-0.20+lexer-dev (= ${binary:Version}),
 librust-lalrpop-0.20+pico-args-dev (= ${binary:Version}),
 librust-lalrpop-0.20+unicode-dev (= ${binary:Version}),
 librust-lalrpop-0.20.2-dev (= ${binary:Version}),
 librust-lalrpop-0.20.2+default-dev (= ${binary:Version}),
 librust-lalrpop-0.20.2+lexer-dev (= ${binary:Version}),
 librust-lalrpop-0.20.2+pico-args-dev (= ${binary:Version}),
 librust-lalrpop-0.20.2+unicode-dev (= ${binary:Version})
Description: Convenient LR(1) parser generator - Rust source code
 Source code for Debianized Rust crate "lalrpop"

Package: lalrpop
Architecture: any
Multi-Arch: allowed
Section: rust
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
 ${cargo:Depends}
Recommends:
 ${cargo:Recommends}
Suggests:
 ${cargo:Suggests}
Provides:
 ${cargo:Provides}
Built-Using: ${cargo:Built-Using}
Static-Built-Using: ${cargo:Static-Built-Using}
Description: Convenient LR(1) parser generator
 This package contains the following binaries built from the Rust crate
 "lalrpop":
  - lalrpop
